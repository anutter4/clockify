Changelog
=========


(unreleased)
------------
- Release 2.0.2 → 2.0.3. [paulossjunior]
- Fix(request): sleeping by 60 seconds after request erro.
  [paulossjunior]
- Docs(documentation): documentation. [paulossjunior]
- Release 2.0.1 → 2.0.2. [paulossjunior]
- Fix(request): retry the connection after 60 seconds. [paulossjunior]
- Release 2.0.0 → 2.0.1. [paulossjunior]
- Fix(search): retrive information about time entry. [paulossjunior]
- Release 1.0.17 → 2.0.0. [paulossjunior]
- Docs(readme): fix documentation. [paulossjunior]

  BREAKING CHANGE:
- Release 1.0.16 → 1.0.17. [paulossjunior]
- Fix(remove): remove test file. [paulossjunior]
- Docs(documentation): readme.md. [paulossjunior]
- Release 1.0.15 → 1.0.16. [paulossjunior]
- X. [paulossjunior]
- Refactor(refactory): refactorig. [paulossjunior]
- Docs(translate-to-english.): documentation how use lib.
  [paulossjunior]
- Modificando. [paulossjunior]
- X. [paulossjunior]
- X. [paulossjunior]
- X. [paulossjunior]
- Corrigindo a criação de projetos. [paulossjunior]
- X. [paulossjunior]
- X. [paulossjunior]
- Atualização do readme. [paulossjunior]
- Criando a função pra criar novos workspace. [Paulo Sérgio dos Santos
  Júnior]
- Criando a função pra criar novos workspace. [Paulo Sérgio dos Santos
  Júnior]
- Criando a função pra criar novos workspace. [Paulo Sérgio dos Santos
  Júnior]
- X. [Paulo Sérgio dos Santos Júnior]
- Modificando a função de buscar workspace. [Paulo Sérgio dos Santos
  Júnior]
- X. [Paulo Sérgio dos Santos Júnior]
- Arquivo de teste. [Paulo Sérgio dos Santos Júnior]
- Criando o settup da lib. [Paulo Sérgio dos Santos Júnior]
- Criando a função para criar projeto. [Paulo Sérgio dos Santos Júnior]
- X. [Paulo Sérgio dos Santos Júnior]
- Editando o readme. [Paulo Sérgio dos Santos Júnior]
- Configurando a lib. [Paulo Sérgio dos Santos Júnior]
- Criando a classe responsável por comunicar com o Clockify. [Paulo
  Sérgio Santos Junior]
- Add new file. [Paulo Sérgio Santos Junior]
- Initial commit. [Paulo Sérgio Santos Junior]


